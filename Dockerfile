FROM jekyll/builder
RUN git clone https://github.com/Mikroways/mikroways.net.git /tmp/site
WORKDIR /tmp/site
RUN chown -R jekyll:jekyll /tmp/site
RUN bundle install
RUN jekyll build

FROM nginx:latest
WORKDIR /usr/share/nginx/html
COPY --from=0 /tmp/site/_site /usr/share/nginx/html/